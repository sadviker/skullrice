# Created by newuser for 5.9

autoload -Uz add-zsh-hook
autoload -Uz vcs_info
precmd() { vcs_info }

zstyle ':vcs_info:*' formats '%b%s-[%F{magenta}[]%f %F{yellow}%b%f]-'

if [ -d "$HOME/.local/bin" ];
	then PATH="$HOME/.local/bin:$PATH"
fi
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

alias ls='lsd'
alias vm-res='sudo systemctl restart libvirtd.service'
alias vm-on='sudo systemctl start libvirtd.service'
alias vm-off='sudo systemctl stop libvirtd.service'

export PS1="%F{#F5A9F2}%n%f%F{white}@%f%F{#D0A9F5}%m%f::%F{#F5A9BC}%~%f${vcs_info_msg_0_} "
